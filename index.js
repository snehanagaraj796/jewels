const menu = [
    {
        id:1,
        title:"Pearl Bangles",
        category: "Bangles",
        img:"./images/i1.jpg",
        desc:"Pearl Bangles are special and looks beautiful.These bangles are classy yet guileless, with an elusive charm about them. They are not traditional, strictly speaking, but they effortlessly bridge the gap between generations.",
        price:"50,304",
    },
    {
        id:2,
        title:"Gold Platted Bangles",
        category:"Bangles",
        img:"./images/i2.jpg",
        desc: "Gold bangles are the wear-everywhere answer to this summer’s pared-down aesthetic",
        price:"78,304",
    },
    {
        id:3,
        title:"Ring-Earings",
        category:"Earings",
        img:"./images/i3.jpeg",
        price:"75,304",
        desc:"An earring is a piece of jewelry attached to the ear via a piercing in the earlobe or another external part of the ear",
    },
    {
        id:4,
        title:"Hanging-Earings",
        category:"Earings",
        img:"./images/i4.jpg",
        price:"90,404",
        desc:"An earring is a piece of jewelry attached to the ear via a piercing in the earlobe or another external part of the ear",
    },
    {
        id:5,
        title:"Choker",
        category:"Chains/Necklace",
        img:"./images/i5.jpg",
        price:"23,804",
        desc:"A gold chain is a piece of jewelry made of gold that we wear around the neck.",
    },
    {
        id:6,
        title:"Necklace",
        category:"Chains/Necklace",
        img:"./images/i6.jpeg",
        price:"80,564",
        desc:"Gold Necklace is a precise embellishing art for your neck with distinctive character, desirable shine, and sheer aura of royalty. ",
    },
    {
        id:7,
        title:"Stylish-Ring",
        category:"Rings",
        img:"./images/i8.jpeg",
        price:"48,304",
        desc:"The Gold Warm Embrace Diamond Ring is inspired by the romantic moments we share with our loved ones that are always etched in our hearts.",
    },
    {
        id:8,
        title:"Plain-Ring",
        category:"Rings",
        img:"./images/i7.jpeg",
        price:"40,200",
        desc:"The Gold Warm Embrace Diamond Ring is inspired by the romantic moments we share with our loved ones that are always etched in our hearts.",

    },
    {
        id:9,
        title:"Girls-Bracelet",
        category:"Bracelets",
        img:"./images/19.jpg",
        price:"35,304",
        desc:"Bracelets contains a silver heart so that girls know that they are loved, an animal bead so that girls know that they are part of a larger pack that has their backs, and a red bead reminding them that they are strong and protected."
    },
    {
        id:10,
        title:"Boys-Bracelet",
        category:"Bracelets",
        img:"./images/i10.jpeg",
        price:"32,306",
        desc:"Charming and classy, young boys look so handsome in our stunning bracelets. The boy's bracelets are marvelous additions to their Sunday Best and still look fantastic with the everyday ensemble. They are perfect gifts for birthdays, graduations.",
    }
]


const c1 = document.querySelector(".c1");
const filterBtns = document. querySelectorAll(".filter-btn")

// load items
window.addEventListener("DOMContentLoaded",function(){
  displayMenuItems(menu);
});
// filter items
filterBtns.forEach(function (btn) {
  btn.addEventListener("click",function(e) {
     const category =  e.currentTarget.dataset.id;
     const menuCategory = menu.filter(function (menuItem) {
    //  console.log(menuItem.category);  
      if (menuItem.category === category){
        return menuItem;
      }
     });
      // console.log (menuCategory);
      if( category === "All"){
        displayMenuItems(menu);
      } else {
        displayMenuItems(menuCategory);
      }    
    });
})
function displayMenuItems (menuItems){
    let displayMenu = menuItems.map(function(item){
      // console.log(item);
      
      return `<article class="menu-item">
        <img src=${item.img} class="photo"alt=${item.tittle}/>
        <div class="item-info">
          <header>
            <h4>${item.title}</h4>
            <h4 class="price">Rs. ${item.price}</h4>
          </header>
            <p class="item-text">
            ${item.desc}
            </p>
        </article>`;
    });
    displayMenu = displayMenu.join("");
    c1.innerHTML = displayMenu;
  }
  